
#include <stdio.h>
#include <stdarg.h>

#include <libdebug.h>
#include <libprintf.h>

void callback(const char* src, size_t n)
{
	fwrite(src, n, 1, stdout);
}

size_t libprintf(const char* fmt, ...)
{
	size_t printed;
	va_list args;
	va_start(args, fmt);
	printed = libvgprintf(callback, fmt, args);
	va_end(args);
	return printed;
}

int main()
{
	ENTER;
	verprintf("Hello, World!\n");
	libprintf("Hello, World!\n");
	libprintf("test character: %c \n", 't');
	libprintf("test string: %s \n", "test!");
	libprintf("test pointer: %p \n", 0xBEEF);
	libprintf("test integer: %i \n", 3);
	EXIT;
	return 0;
}

