

CC = gcc
CPPFLAGS = -I . -I ..
CPPFLAGS += -D DEBUG=1
CFLAGS += -Wall -Werror=implicit-function-declaration

# LDLIBS = -lm

TARGET = main

default: $(TARGET)

install: ~/bin/$(TARGET)

~/bin/$(TARGET): $(TARGET)
	cp $(TARGET) ~/bin/

run: $(TARGET)
	./$(TARGET) $(ARGS)

valrun: $(TARGET)
	valgrind ./$(TARGET) $(ARGS)

srclist.mk:
	find -name '*.c' ! -path './#*' | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

deps += libchar
deps += libprintf
deps += libdebug

DEPS = $(foreach dep,$(deps),../$(dep).a)

installdeps:
	$(foreach dep,$(deps),$(MAKE) -C ../$(dep) install &&) true

OBJS = $(SRCS:.c=.o)
DEPENDS = $(SRCS:.c=.mk)

$(TARGET): $(OBJS) $(DEPS)
	$(CC) $(LDFLAGS) $(OBJS) $(DEPS) $(LOADLIBES) $(LDLIBS) -o $@

%.l.mk: %.l
	echo "$(basename $<).c: $<" > $@

%.y.mk: %.y
	echo "$(basename $<).c: $<" > $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $<

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

.PHONY: clean deep-clean

clean:
	rm $(OBJS) $(DEPENDS) srclist.mk $(TARGET)

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -executable -delete

include $(DEPENDS)

